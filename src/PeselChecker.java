import java.util.Scanner;

public class PeselChecker {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String pesel = setPesel();
        peselChecker(pesel);
    }

    private static void peselChecker(String pesel) {
        if (checkSum(pesel)) {
            getBirthDay(pesel);
            getSex(pesel);
            getNumber(pesel);
        } else {
            System.out.println("Błędny PESEL!!");
        }
    }

    private static String setPesel() {
        String pesel;
        do {
            System.out.println("Podaj PESEL: ");
            pesel = scanner.nextLine();
            if (pesel.length() != 11) {
                System.out.println("Błędna liczba cyfr.");
            }
        } while (pesel.length() != 11);


        return pesel;
    }

    private static void getNumber(String pesel) {
        System.out.println("Jesteś " + pesel.charAt(6) + pesel.charAt(7) + pesel.charAt(8) + " osobą o tym imieniu i nazwisku w roku swoich urodzin.");
    }

    private static void getSex(String pesel) {
        if (pesel.charAt(9) % 2 == 0) {
            System.out.println("Płeć: Kobieta");
        } else {
            System.out.println("Płeć: Mężczyzna");
        }
    }

    private static void getBirthDay(String pesel) {

        String month;
        if (pesel.charAt(2) == '8' || pesel.charAt(2) == '9') {
            if (pesel.charAt(2) == '8') {
                month = getMonth(pesel.charAt(3));
            } else {
                month = getMonthPlus(pesel.charAt(3));
            }
            System.out.println("Rok: 18" + pesel.charAt(0) + pesel.charAt(1) + ", Miesiąc: " + month + ", Dzień: " + pesel.charAt(4) + pesel.charAt(5));
        }
        if (pesel.charAt(2) == '0' || pesel.charAt(2) == '1') {
            if (pesel.charAt(2) == '0') {
                month = getMonth(pesel.charAt(3));
            } else {
                month = getMonthPlus(pesel.charAt(3));
            }
            System.out.println("Rok: 19" + pesel.charAt(0) + pesel.charAt(1) + ", Miesiąc: " + month + ", Dzień: " + pesel.charAt(4) + pesel.charAt(5));
        }
        if (pesel.charAt(2) == '2' || pesel.charAt(2) == '3') {
            if (pesel.charAt(2) == '2') {
                month = getMonth(pesel.charAt(3));
            } else {
                month = getMonthPlus(pesel.charAt(3));
            }
            System.out.println("Rok: 20" + pesel.charAt(0) + pesel.charAt(1) + ", Miesiąc: " + month + ", Dzień: " + pesel.charAt(4) + pesel.charAt(5));
        }
    }

    private static String getMonth(char peselChar) {
        String month = null;
        switch (peselChar) {
            case '1':
                month = "Styczeń";
                break;
            case '2':
                month = "Luty";
                break;
            case '3':
                month = "Marzec";
                break;
            case '4':
                month = "Kwiecień";
                break;
            case '5':
                month = "Maj";
                break;
            case '6':
                month = "Cerwiec";
                break;
            case '7':
                month = "Lipiec";
                break;
            case '8':
                month = "Sierpień";
                break;
            case '9':
                month = "Wrzesień";
                break;
        }
        return month;
    }

    private static String getMonthPlus(char peselChar) {
        String month = null;
        switch (peselChar) {
            case '0':
                month = "Październik";
                break;
            case '1':
                month = "Listopad";
                break;
            case '2':
                month = "Grudzień";
                break;
        }
        return month;
    }

    private static boolean checkSum(String pesel) {
        int[] ratio = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};
        int result = 0;
        for (int i = 0; i < 10; i++) {
            result += ((ratio[i] * ((int) pesel.charAt(i) - 48)));
        }
        result = 1000 - result;
        String last = Integer.toString(result);
        if (last.charAt(last.length() - 1) == pesel.charAt(10)) {
            return true;
        } else {
            return false;
        }
    }

}
